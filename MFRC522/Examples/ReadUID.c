/*  Example code to use the MFRC522 module */

// Include MCU specific libs
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
// Include the MFRC522 lib
#include "mfrc522.h"

int main(void)
{
  uint8_t uidBuffer[10];

  while (1) {
    // Check for a new RFID card
    if (PICC_IsNewCardPresent() == STATUS_OK) {
      // Read out the UID buffer from the RFID card
      PICC_ReadCardSerial(uidBuffer);
    }
  }

}
