/*  This code is used to drive NRF24 wireless modules on a STM32F7 mcu */

#include "nrf24l01.h"

#if NRF24_USE_IRQ
// Binary IRQ semaphore
binary_semaphore_t NRF24SemIRQ;
#endif

#if NRF24_USE_IRQ && !NRF24_OWN_IRQ_THD

/**
 * @brief NRF24 IRQ handler thread
 * 
 * You can copy this to make your own thread in your
 * main file
 **/
static THD_WORKING_AREA(NRF24_thread_wa, 256);
static THD_FUNCTION(NRF24_thread, p) {

  (void)p;
	// Thread identifier
  chRegSetThreadName("NRF24 thread");
  while (true) {
		// Wait for the semaphore signal
		chBSemWait(&NRF24SemIRQ);
    // Read out the NRF24 status register
    Status = NRF24L01_Read_Status_Reg();
    // Else if structure to handle all events
    if(Status & RX_DR) // When there is data ready in the receive FIFO
      __NOP();
    else if(Status & TX_DS)  // When the data was sent successfully after a transmit operation
      __NOP();
    else if(Status & MAX_RT) // The data was not sent after the maximum configured amount of attempts
      __NOP();
  }
}
#endif

/**
 * @brief Read the value of a register
 *
 * @param Reg Register to read the value from
 *
 * @return Value read from the register
 **/
uint8_t NRF24L01_Read_Reg(uint8_t Reg) {
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
	uint8_t Result[2]; // Return values
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
	uint8_t Data[2]; // Data to send
	// The data buffer is filled with the register value and
	// a NOP instruction to read out the register
	Data[0] = Reg;
	Data[1] = NOP;
	// SPI tranmission
	spiAcquireBus(&SPID5);              			// Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(Data, 2);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  				// Select the slave (CS low)
	spiExchange(&SPID5, 2, Data, Result);// Send DATA and receive te response
	spiUnselect(&SPID5);                			// Unselect the slave (CS High)
	dmaBufferInvalidate(Result, 2);			// Invalidate the Cache lines from the receive buffer
	spiReleaseBus(&SPID5);              			// Release the bus for others to use (Unlock mutex)
  // Return the value of the register (Result[0] contains the status register value)
  return Result[1];
}


/**
 * @brief Read the value of a register longer than one byte
 *
 * @param Reg Register to read the value from
 * @param Buf The pointer to a buffer to store the read values in
 * @param RegSize The size of the register to read from
 *
 * @return Value read from the status register
 **/
uint8_t NRF24L01_Read_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize) {
	#if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Result[RegSize+1]; // The return value of the register will be one larger than the
  // specified size. This because the status register is also read
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Data[RegSize+1]; // The data register must be one larger too
  // With a for loop the data register is filled with NOP instructions to read out the register
  for(int i=0; i < RegSize+1; i++)
    Data[i] = NOP;
  Data[0] = Reg; // Also put the register value int he buffer
	// SPI tranmission
	spiAcquireBus(&SPID5);              					// Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(Data, RegSize+1);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  					// Select the slave (CS low)
	spiExchange(&SPID5, RegSize+1, Data, Result);	// Send DATA and receive te response
	spiUnselect(&SPID5);                					// Unselect the slave (CS High)
	dmaBufferInvalidate(Result, RegSize+1);			// Invalidate the Cache lines from the receive buffer
	spiReleaseBus(&SPID5);              					// Release the bus for others to use (Unlock mutex)
  // Afther the receive the status register is ditched
  for (int i=0; i < RegSize; i++)
  //Shift all elements to the left
    Buf[i] = Result[i+1];
  // Return the status register value
  return Result[0];
}

/**
 * @brief Write the specified value to the specified register
 *
 * @param The register to write to
 * @param Value to write to the register
 *
 * @return Value of the status register
*/
uint8_t NRF24L01_Write_Reg(uint8_t Reg, uint8_t Value) {
	#if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Result[2]; // Return values
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Data[2]; // Data to send
  // Fill the data register with the register adress and the value to write
  Data[0] = (W_REGISTER | Reg);
  Data[1] = Value;
	// SPI tranmission
	spiAcquireBus(&SPID5);              			// Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(Data, 2);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  			// Select the slave (CS low)
	spiExchange(&SPID5, 2, Data, Result);		// Send DATA and receive te response
	spiUnselect(&SPID5);                			// Unselect the slave (CS High)
	dmaBufferInvalidate(Result, 2);			// Invalidate the Cache lines from the receive buffer
	spiReleaseBus(&SPID5);              			// Release the bus for others to use (Unlock mutex)
  // Return the status register value
  return Result[0];
}


/**
 * @brief Write the value in a buffer to a register that is larger than one byte
 *
 * @param Reg Register to write the buffer to
 * @param Buf The pointer to a buffer containing the values for the register
 * @param RegSize The size of the register writing to
 *
 * @return Value of the status regiser
*/
uint8_t NRF24L01_Write_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize) {
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Result[RegSize+1]; // Return values, one larger than the register
  // size to read the status register also
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Data[RegSize+1]; // Data to send is one larger to also send the register adress
  // A for loop to put the values form the buffer into our data buffer
  for(int i=0;i<RegSize;i++)
    Data[i+1] = Buf[i];
  Data[0] = (W_REGISTER | Reg); // Also write the register adress in the data buffer
	// SPI tranmission
	spiAcquireBus(&SPID5);              			// Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(Data, RegSize+1);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  			// Select the slave (CS low)
	spiExchange(&SPID5, RegSize+1, Data, Result);	// Send DATA and receive te response
	spiUnselect(&SPID5);                			// Unselect the slave (CS High)
	dmaBufferInvalidate(Result, RegSize+1);			// Invalidate the Cache lines from the receive buffer
	spiReleaseBus(&SPID5);              			// Release the bus for others to use (Unlock mutex)
  // Return the status register value
  return Result[0];
}

/**
 * @brief Send a NOP instruction to read out the status register
 *
 * @return Value of the status register
 */
uint8_t NRF24L01_Read_Status_Reg(void) {
  #if defined(__GNUC__)
  __attribute__((aligned (32)))
  #endif
  uint8_t Result, Data;
  // Fill the data with NOP instruction
  Data = NOP;
	// SPI tranmission
	spiAcquireBus(&SPID5);              			// Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(Data, 1);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  			// Select the slave (CS low)
	spiExchange(&SPID5, 1, &Data, &Result);	// Send DATA and receive te response
	spiUnselect(&SPID5);                			// Unselect the slave (CS High)
	dmaBufferInvalidate(Result, 1);			// Invalidate the Cache lines from the receive buffer
	spiReleaseBus(&SPID5);              			// Release the bus for others to use (Unlock mutex)
  // Return the status register
  return Result;
}

/**
 * @brief Send a command to the NRF-module
 *
 * @return The command to be send
 */
void NRF24L01_Send_Cmd(uint8_t cmd) {
	// SPI tranmission
	spiAcquireBus(&SPID5);              // Get access over the SPI bus (Lock mutex)
	spiStart(&SPID5, &NRF24_hs_spicfg); 	// Configure the SPI peripheral with the NRF24 config
	dmaBufferFlush(cmd, 1);				// Flush the buffer from RAM to Cache
	spiSelect(&SPID5);                  // Select the slave (CS low)
	spiSend(&SPID5, 1, &cmd);  				// Send the data
	spiUnselect(&SPID5);                // Unselect the slave (CS High)
	spiReleaseBus(&SPID5);              // Release the bus for others to use (Unlock mutex)
}

/**
 * @brief Enable shockburst for the desired channels
 *
 * @param ACK The channels to enable in hex form
 *
 */
void NRF24L01_Enable_AutoACK(uint8_t ACK){
  NRF24L01_Write_Reg(EN_AA, ACK);
}

/**
 * @brief Read out the received power bit
 *
 * @return The RPD bit
*/
uint8_t NRF24L01_Read_RPD(void) {
	return (NRF24L01_Read_Reg(RPD));
}

/**
 * @brief Power down the NRF module
*/
void NRF24L01_Power_Down(void) {
  uint8_t Result; // Needed to read out the config register

	Result = NRF24L01_Read_Reg(CONFIG) & 0x7D; // Read out the config register and clear the PWR_UP bit (bit 1)
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Power up the NRF module
*/
void NRF24L01_Power_Up(void) {
  uint8_t Result; // Needed to read out the config register

	Result = NRF24L01_Read_Reg(CONFIG) | 0x2; // Read out the config register and set the PWR_UP bit (bit 1)
	NRF24L01_Write_Reg(CONFIG, Result);
}


/**
 * @brief Set the desired channel for the module to communicate on
 *
 * F0 = 2400 + RF_CH [MHz]
 *
 * @param Channel The RF_CH value int he upper formula, can be between 0 and 125
*/
void NRF24L01_Set_Channel(uint8_t Channel) {
  if(Channel > 125) // Make sure Channel is not above 125
    Channel = 125;
	NRF24L01_Write_Reg(RF_CH, Channel);
}

/**
 * @brief Set the RF output power in TX mode
 *
 * @param Power The power in TX mode (_0dBm, _6dBm, _12dBm, _18dBm)
 */
void NRF24L01_Set_Power(uint8_t Power) {
  uint8_t Result = NRF24L01_Read_Reg(RF_SETUP) & 0xB9;
  NRF24L01_Write_Reg(RF_SETUP, Result | Power);
}

/**
 * @brief Set the modules data rate
 *
 * @param DataRate The desired data rate (_1Mbps, _2Mbps, _250Kbps)
 */
void NRF24L01_Set_Data_Rate(uint8_t DataRate) {
  uint8_t Result = NRF24L01_Read_Reg(RF_SETUP) & 0x97; // Clear the RF_DR bits
  NRF24L01_Write_Reg(RF_SETUP, (Result | DataRate)); // Write the data rate
}

/**
 * @brief Set the desired address width (between 3 and 5)
 *
 * @param AddressWidth The desired width between 3 and 5
*/
uint8_t NRF24L01_Set_Address_Width(uint8_t AddressWidth) {
  if(AddressWidth >= 3){ // Make sure the width isn't smaller than 3
    AddressWidth -= 0x2;
    NRF24L01_Write_Reg(SETUP_AW, AddressWidth);
    return 1;
  }
  return -1; // Failed due to wrong parameters
}

/**
 * @brief Put the NRF module in transmit mode
*/
void NRF24L01_Set_Mode_TX(void) {
  uint8_t Result; // Read out the config register first

	Result = NRF24L01_Read_Reg(CONFIG) & 0x7E; // Read out the config register and clear out the PRIM_RX bit 0
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Put the NRF module in receive mode
*/
void NRF24L01_Set_Mode_RX(void) {
  uint8_t Result; // Read out the config register first

	Result = NRF24L01_Read_Reg(CONFIG) | 0x1; // Read out the config register and set the PRIM_RX bit 0
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Enables and configures a specified receiving pipe
 *
 * @param PipeNumber  The number of the pipe to be configured
 * @param PipeAddress The address the pipe needs to get
 * @param AddressSize The size of the address
 * @param PayloadSize The size of the payload that will be received in this pipe
 *
*/
void NRF24L01_Set_RX_Pipe(uint8_t PipeNumber, uint8_t *PipeAddress, uint8_t AddressSize, uint8_t PayloadSize) {
	uint8_t Result;
	// Read out the EN_RXADDR register to keep all the active pipes enabled
	Result = NRF24L01_Read_Reg(EN_RXADDR);
	NRF24L01_Write_Reg(EN_RXADDR, Result | (1 << PipeNumber)); // Activate the specified pipe
	// Write the payload size in the appropriate RX_PW register
	NRF24L01_Write_Reg((RX_PW_P0 + PipeNumber), PayloadSize);
	if(PipeNumber <= 1)
	  NRF24L01_Write_Reg_Buf((RX_ADDR_P0 + PipeNumber), PipeAddress, (uint16_t)AddressSize); // Write the PipeAdress
	else
	  NRF24L01_Write_Reg((RX_ADDR_P0 + PipeNumber), PipeAddress[0]);
}

/**
 * @brief Disable all pipes
*/
void NRF24L01_Disable_All_Pipes(void) {
	NRF24L01_Write_Reg(EN_RXADDR, 0);
}

/**
 * @brief Enable all pipes
*/
void NRF24L01_Enable_All_Pipes(void) {
	NRF24L01_Write_Reg(EN_RXADDR, 0x3F);
}

/**
 *
 * @brief Enable the specified interrupt
 *
 * @param Interrupt The interrupt that has to be enabled
 * (RX_DR/TX_DS/MAX_RT)
 *
 */
void NRF24L01_Enable_Interrupt(uint8_t Interrupt){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config |= Interrupt;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Disable the specified interrupt
 *
 * @param Interrupt The interrupt that has to be disabled
 * (RX_DR/TX_DS/MAX_RT)
 *
 */
void NRF24L01_Disable_Interrupt(uint8_t Interrupt){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config &= ~Interrupt;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Enable all the interrupts
 *
 */
void NRF24L01_Enable_All_Interrupts(void){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config |= RX_DR | TX_DS | MAX_RT;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Disable all interrupts
 *
 */
void NRF24L01_Disable_All_Interrupts(void){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config &= ~(RX_DR | TX_DS | MAX_RT);
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Clear all interrupts
 *
 * @return STATUS Register
 *
 */
uint8_t NRF24L01_Clear_All_Interrupts(void) {
	return NRF24L01_Write_Reg(STATUS, RX_DR | TX_DS | MAX_RT);
}

/**
 * @brief Set the transmit address of this module
 *
 * @param TransmitAddress The address
 * @param AddressSize The size of the given address (must be the size set in the SETUP_AW register)
 *
*/
void NRF24L01_Set_TX_Address(uint8_t *TransmitAddress, uint16_t AddressSize) {
	NRF24L01_Write_Reg_Buf(TX_ADDR, TransmitAddress, AddressSize);
}

/**
 * @brief Flush the transmit buffer
*/
void NRF24L01_Flush_TX(void) {
  NRF24L01_Send_Cmd(FLUSH_TX);
}

/**
 * @brief Flush the receive buffer
*/
void NRF24L01_Flush_RX(void) {
  NRF24L01_Send_Cmd(FLUSH_RX);
}

/**
 * @brief Initialize the pins for the NRF module
 */
void Pins_Init(void){
	// Setup SPI GPIO pins
	palSetPadMode(NRF24_SPI_GPIO_PORT, NRF24_SPI_SCK_PIN, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST); 		// SCK pin
	palSetPadMode(NRF24_SPI_GPIO_PORT, NRF24_SPI_MISO_PIN, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST);		// MISO pin
	palSetPadMode(NRF24_SPI_GPIO_PORT, NRF24_SPI_MOSI_PIN, PAL_MODE_ALTERNATE(5) | PAL_STM32_OSPEED_HIGHEST); 	// MOSI pin
	palSetPadMode(NRF24_SPI_CS_PORT, NRF24_SPI_CS_PIN, PAL_MODE_OUTPUT_PUSHPULL);	// Chip select
	// Set up Chip enable and interrupt pin
	palSetPadMode(NRF24_CE_PORT, NRF24_CE_PIN, PAL_MODE_OUTPUT_PUSHPULL);			// Chip enable
#if NRF24_USE_IRQ
	palSetPadMode(NRF24_IRQ_PORT, NRF24_IRQ_PIN, PAL_MODE_INPUT);							// IRQ pin
#endif
}

/**
 * @brief Init routine to initialize the Pins and SPI peripheal and do a basic setup of the module
 * 
 * With this function all the pins are configured and the SPI is also configured.
 * The NRF module is loaded with basic settings that can be configured using the input paramters
 * of this function. Further configuration of the device can be done with all the other functions
 * available.
 * What is standard set with this function:
 * Shockburst is enabled for all channels
 * The transmit address is set togheter with a receive pipe for auto acknowledgement
 * The device is put in transmit mode
 * The device is powered up
 * Other things are configured with parameters, see below
 *
 * @param Channel         The channel to be set for receive and transmit (0..125)
 * @param DataRate        The data rate to be used (1Mbps, 2Mbps, 250Kbps)
 * @param TransmitAddress The address to be used for transmitting 
 * @param AddressWidth    The width of the address (3..5)
 * @param PayloadSize     The size of the payloads to be transmitted
 *
*/
void NRF24L01_Init(uint8_t Channel, uint8_t DataRate,
		uint8_t *TransmitAddress, uint8_t AddressWidth, uint8_t PayloadSize) {
  // Initalize the pins and the SPI peripheral
  Pins_Init();
#if NRF24_USE_IRQ
	// Initialize the EXTI pin
	extStart(&EXTD1, &extcfg);
	extChannelEnable(&EXTD1, NRF24_IRQ_PIN);
#endif
#if NRF24_USE_IRQ && !NRF24_OWN_IRQ_THD
	// Create a static thread to listen to interrupt requests
	chThdCreateStatic(NRF24_thread_wa, sizeof(NRF24_thread_wa),
                    NORMALPRIO + 1, NRF24_thread, NULL);
#endif

	// Enable Enhanced ShockBurst for all channels
	NRF24L01_Enable_AutoACK(0x3F);

	// Set the maximum output power (0dBm) and set the datarate
	NRF24L01_Write_Reg(RF_SETUP, (0x6 | DataRate));
	// Set the used overal address width
	NRF24L01_Set_Address_Width(AddressWidth);
	// To use Auto acknowledgement, pipe 0 must have the transmit address
	NRF24L01_Set_RX_Pipe(0, TransmitAddress, AddressWidth, PayloadSize);
	// The channel to be used for tranmitting and receiving
	NRF24L01_Set_Channel(Channel);
	NRF24L01_Set_TX_Address(TransmitAddress, AddressWidth); // Set Transmit address

	// Enable CRC, set CRC to 2 bytes, put into transmit mode and power up the module
	NRF24L01_Write_Reg(CONFIG, 0xE);

	chThdSleepMilliseconds(2); // Delay for 2ms (1,5ms specified) after module power-up
}

/**
 Turn on transmitter, and transmits the data loaded into the buffer
*/
void NRF24L01_Send_Payload(uint8_t *Buf, uint16_t BufSize) {
  uint8_t Result;
  
  if(NRF24L01_Read_Status_Reg() & MAX_RT) // Clear all interrupts if MAX_RT is set
    NRF24L01_Clear_All_Interrupts();
  // Make sure the device is in TX_MODE and power it up
  Result = NRF24L01_Read_Reg(CONFIG) & 0x7E;
  NRF24L01_Write_Reg(CONFIG, (Result | 0x2));
  
  NRF24L01_Write_TX_Buf(Buf, BufSize);
  
  // Let the CE-pin go high for at least 10us
	CE_L();
	CE_H();
	chThdSleepMilliseconds(1); // Delay 1ms (10us is specified but the HAL minimum delay is in ms)
	              // Also I am too lazy to implement my own delay based on timers
	CE_L();
}

/**
 * @brief Transmit a payload (this function automaticly puts the module in TX mode)
 *
 * @param Buf       The payload that has to be transmitted
 * @param BufSize   The size of the payload to be transmitted
 *
 * @return Returns 1 on success and 0 when transmit fails
 *
*/
uint8_t NRF24L01_Send(uint8_t *Buf, uint16_t BufSize) {
  NRF24L01_Set_Mode_TX(); // Go into transmit mode
  // Send the payload
  NRF24L01_Send_Payload(Buf, BufSize);
  //Wait for the payload to be send
  while ((NRF24L01_Read_Status_Reg() & TX_DS) != TX_DS){
      if((NRF24L01_Read_Status_Reg() & MAX_RT) == MAX_RT){
          NRF24L01_Clear_All_Interrupts();
          NRF24L01_Flush_TX();
          return 0; //Failed
      }
  }
	NRF24L01_Clear_All_Interrupts();
  return 1; // Success
}

/**
 * @brief Write the transmit payload buffer
 *
 * @param PayloadBuffer The data to be written into the payload buffer
 * @param PayloadSize   The size of the data to be written into the buffer
 *
*/
void NRF24L01_Write_TX_Buf(uint8_t *PayloadBuffer, uint16_t PayloadSize) {
	NRF24L01_Write_Reg_Buf(W_TX_PAYLOAD, PayloadBuffer, PayloadSize);
}

/**
 * @brief Read the data from the receive payload buffer
 *
 * @param ReceiveBuf  Data received
 * @param PayloadSoze Size of the received data
 *
*/
void NRF24L01_Read_RX_Buf(uint8_t *ReceiveBuf, uint16_t PayloadSize) {
	NRF24L01_Read_Reg_Buf(R_RX_PAYLOAD, ReceiveBuf, PayloadSize);
}

#if NRF24_USE_IRQ
/*
 *
 * NRF24_IRQ handler routine
 *
*/
void extcb15(EXTDriver *extp, expchannel_t channel) {
  (void)extp;
  (void)channel;
	// If the IRQ pin is high
  if (!palReadPad(NRF24_IRQ_PORT, NRF24_IRQ_PIN)) {
	  chSysLockFromISR();
		// Signal the NRF24 thread to handle the interrupt
		chBSemSignalI(&NRF24SemIRQ);
	  chSysUnlockFromISR();
	}
}
#endif
