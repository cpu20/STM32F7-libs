/* Example to receive data interrupt based method */

// ChibiOS libs
#include "ch.h"
#include "hal.h"
// Include the nRF24 lib
#include "nrf24l01.h"

uint8_t RxBuf[_Buffer_Size]; // Global Receive buffer

/**
 * @brief NRF24 IRQ handler thread
 * Custom version
 **/
static THD_WORKING_AREA(NRF24_thread_wa, 256);
static THD_FUNCTION(NRF24_thread, p) {
  (void)p;
  // Thread identifier
  chRegSetThreadName("NRF24 thread");
  while (true) {
  // Wait for the semaphore signal
  chBSemWait(&NRF24SemIRQ);
    // Read out the NRF24 status register
    uint8_t Status = NRF24L01_Read_Status_Reg();
    // Else if structure to handle all events
    if(Status & RX_DR) { // When there is data ready in the receive FIFO
      CE_L(); // Stop receiving
      NRF24L01_Read_RX_Buf(RxBuf, _Buffer_Size);
      NRF24L01_Clear_All_Interrupts();
      // Update battery voltage bars
      UpdateSensorStatus(Status & RX_P_NO);
      CE_H(); // Start receiving again
    }
    else if(Status & TX_DS)  // When the data was sent successfully after a transmit operation
      __NOP();
    else if(Status & MAX_RT) // The data was not sent after the maximum configured amount of attempts
      __NOP();
  }
}

void main(void)
{

	halInit();
  chSysInit();

  // The transmit address
  uint8_t TX_Address[_Address_Width] = { 0xD2, 0xF0, 0xF0, 0xF0, 0xF0 };
  // The nRF24 module can receive from 4 different modules with different addresses (pipes)
  uint8_t Module1_Address[_Address_Width] = { 0xE1, 0xF0, 0xF0, 0xF0, 0xF0 };
  uint8_t Module2_Address = 0xE2;
  uint8_t Module3_Address = 0xE3;
  uint8_t Module4_Address = 0xE4;

  // Init the NRF24 unit with basic parameters
  NRF24L01_Init(_Channel, _1Mbps, TX_Address, _Address_Width, _Buffer_Size);
  // Setup all the receiving pipes
  NRF24L01_Set_RX_Pipe(1, Module1_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(2, &Module2_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(3, &Module3_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(4, &Module4_Address, _Address_Width, _Buffer_Size);
  // Flush the buffers at setup
  NRF24L01_Flush_TX();
  NRF24L01_Flush_RX();
  // Clear all interrupts
  NRF24L01_Clear_All_Interrupts();

  NRF24L01_Set_Mode_RX(); // Go into receive mode
  CE_H(); // Place the CE pin high to start receiving

  while(1){
		// Empty main thread.
    chThdSleepMilliseconds(500);
  }

}
