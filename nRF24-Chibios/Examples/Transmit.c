/*  Example code to transmit data with the nRF24 module */

// ChibiOS libs
#include "ch.h"
#include "hal.h"
// Include the nRF24 lib
#include "nrf24l01.h"

void Error(void){
  while(1){
    __NOP(); // Put code for debugging here
  }
}

int main(void)
{

  // Transmit buffer
  uint8_t TxBuf[_Buffer_Size] = { 0x4, 0x3, 0x2, 0x1 };
  // The transmit address
  uint8_t TX_Address[_Address_Width] = { 0xD2, 0xF0, 0xF0, 0xF0, 0xF0 };
  // The nRF24 module can receive from 4 different modules with different addresses (pipes)
  uint8_t Module1_Address[_Address_Width] = { 0xE1, 0xF0, 0xF0, 0xF0, 0xF0 };
  uint8_t Module2_Address = 0xE2;
  uint8_t Module3_Address = 0xE3;
  uint8_t Module4_Address = 0xE4;

  // Init the NRF24 unit with basic parameters
  NRF24L01_Init(_Channel, _1Mbps, TX_Address, _Address_Width, _Buffer_Size);
  // Setup all the receiving pipes
  NRF24L01_Set_RX_Pipe(1, Module1_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(2, &Module2_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(3, &Module3_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(4, &Module4_Address, _Address_Width, _Buffer_Size);
  // Flush the buffers at setup
  NRF24L01_Flush_TX();
  NRF24L01_Flush_RX();
  // Clear all interrupts
  NRF24L01_Clear_All_Interrupts();

  NRF24L01_Set_Mode_TX(); // Go into transmit mode

  if(NRF24L01_Send(&TxBuf, _Buffer_Size))
    return 1;
  else
    Error();

}
