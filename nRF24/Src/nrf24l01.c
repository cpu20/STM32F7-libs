/*  This code is used to drive NRF24 wireless modules on a STM32F7 mcu */

#include "nrf24l01.h"

// Typedef used to initialize the SPI interface
static SPI_HandleTypeDef InitSPI;

// Registers AFRL and AFRH are specified differently when using CHIBIOS
#if !GFX_USE_OS_CHIBIOS
  #define AFRL  AFR[0]
  #define AFRH  AFR[1]
#endif

/**
 * @brief Read the value of a register
 *
 * @param Reg Register to read the value from
 *
 * @return Value read from the register
 **/
uint8_t NRF24L01_Read_Reg(uint8_t Reg) {
  uint8_t Result[2]; // Return values
  uint8_t Data[2]; // Data to send
  // The data buffer is filled with the register value and
  // a NOP instruction to read out the register
  Data[0] = Reg;
  Data[1] = NOP;
  // Execute the SPI TransmitReceive
  CSN_L();
  HAL_SPI_TransmitReceive(&InitSPI, Data, Result, 2, 1000);
  CSN_H();
  // Return the value of the register (Result[0] contains the status register value)
  return Result[1];
}


/**
 * @brief Read the value of a register longer than one byte
 *
 * @param Reg Register to read the value from
 * @param Buf The pointer to a buffer to store the read values in
 * @param RegSize The size of the register to read from
 *
 * @return Value read from the status register
 **/
uint8_t NRF24L01_Read_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize) {
  uint8_t Result[RegSize+1]; // The return value of the register will be one larger than the
  // specified size. This because the status register is also read
  uint8_t Data[RegSize+1]; // The data register must be one larger too
  // With a for loop the data register is filled with NOP instructions to read out the register
  for(int i=0; i < RegSize+1; i++)
    Data[i] = NOP;
  Data[0] = Reg; // Also put the register value int he buffer
  // Execute the SPI TransmitReceive
  CSN_L();
  HAL_SPI_TransmitReceive(&InitSPI, Data, Result, RegSize+1, 1000);
  CSN_H();
  // Afther the receive the status register is ditched
  for (int i=0; i < RegSize; i++)
  //Shift all elements to the left
    Buf[i] = Result[i+1];
  // Return the status register value
  return Result[0];
}

/**
 * @brief Write the specified value to the specified register
 *
 * @param The register to write to
 * @param Value to write to the register
 *
 * @return Value of the status register
*/
uint8_t NRF24L01_Write_Reg(uint8_t Reg, uint8_t Value) {
  uint8_t Result[2]; // Return values
  uint8_t Data[2]; // Data to send
  // Fill the data register with the register adress and the value to write
  Data[0] = (W_REGISTER | Reg);
  Data[1] = Value;
  // Exectue the SPI TransmitReceive
  CSN_L();
  HAL_SPI_TransmitReceive(&InitSPI, Data, Result, 2, 1000);
  CSN_H();
  // Return the status register value
  return Result[0];
}


/**
 * @brief Write the value in a buffer to a register that is larger than one byte
 *
 * @param Reg Register to write the buffer to
 * @param Buf The pointer to a buffer containing the values for the register
 * @param RegSize The size of the register writing to
 *
 * @return Value of the status regiser
*/
uint8_t NRF24L01_Write_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize) {
  uint8_t Result[RegSize+1]; // Return values, one larger than the register
  // size to read the status register also
  uint8_t Data[RegSize+1]; // Data to send is one larger to also send the register adress
  // A for loop to put the values form the buffer into our data buffer
  for(int i=0;i<RegSize;i++)
    Data[i+1] = Buf[i];
  Data[0] = (W_REGISTER | Reg); // Also write the register adress in the data buffer
  // Execute the SPI TransmitReceive
  CSN_L();
  HAL_SPI_TransmitReceive(&InitSPI, Data, Result, RegSize+1, 1000);
  CSN_H();
  // Return the status register value
  return Result[0];
}

/**
 * @brief Send a NOP instruction to read out the status register
 *
 * @return Value of the status register
 */
uint8_t NRF24L01_Read_Status_Reg(void) {
  uint8_t Result, Data;
  // Fill the data with NOP instruction
  Data = NOP;
  CSN_L();
  HAL_SPI_TransmitReceive(&InitSPI, &Data, &Result, 1, 1000);
  CSN_H();
  // Return the status register
  return Result;
}

/**
 * @brief Enable shockburst for the desired channels
 *
 * @param ACK The channels to enable in hex form
 *
 */
void NRF24L01_Enable_AutoACK(uint8_t ACK){
  NRF24L01_Write_Reg(EN_AA, ACK);
}

/**
 * @brief Read out the received power bit
 *
 * @return The RPD bit
*/
uint8_t NRF24L01_Read_RPD(void) {
	return (NRF24L01_Read_Reg(RPD));
}

/**
 * @brief Power down the NRF module
*/
void NRF24L01_Power_Down(void) {
  uint8_t Result; // Needed to read out the config register

	Result = NRF24L01_Read_Reg(CONFIG) & 0x7D; // Read out the config register and clear the PWR_UP bit (bit 1)
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Power up the NRF module
*/
void NRF24L01_Power_Up(void) {
  uint8_t Result; // Needed to read out the config register

	Result = NRF24L01_Read_Reg(CONFIG) | 0x2; // Read out the config register and set the PWR_UP bit (bit 1)
	NRF24L01_Write_Reg(CONFIG, Result);
}


/**
 * @brief Set the desired channel for the module to communicate on
 *
 * F0 = 2400 + RF_CH [MHz]
 *
 * @param Channel The RF_CH value int he upper formula, can be between 0 and 125
*/
void NRF24L01_Set_Channel(uint8_t Channel) {
  if(Channel > 125) // Make sure Channel is not above 125
    Channel = 125;
	NRF24L01_Write_Reg(RF_CH, Channel);
}

/**
 * @brief Set the RF output power in TX mode
 *
 * @param Power The power in TX mode (_0dBm, _6dBm, _12dBm, _18dBm)
 */
void NRF24L01_Set_Power(uint8_t Power) {
  uint8_t Result = NRF24L01_Read_Reg(RF_SETUP) & 0xB9;
  NRF24L01_Write_Reg(RF_SETUP, Result | Power);
}

/**
 * @brief Set the modules data rate
 *
 * @param DataRate The desired data rate (_1Mbps, _2Mbps, _250Kbps)
 */
void NRF24L01_Set_Data_Rate(uint8_t DataRate) {
  uint8_t Result = NRF24L01_Read_Reg(RF_SETUP) & 0x97; // Clear the RF_DR bits
  NRF24L01_Write_Reg(RF_SETUP, (Result | DataRate)); // Write the data rate
}

/**
 * @brief Set the desired address width (between 3 and 5)
 *
 * @param AddressWidth The desired width between 3 and 5
*/
uint8_t NRF24L01_Set_Address_Width(uint8_t AddressWidth) {
  if(AddressWidth >= 3){ // Make sure the width isn't smaller than 3
    AddressWidth -= 0x2;
    NRF24L01_Write_Reg(SETUP_AW, AddressWidth);
    return 1;
  }
  return -1; // Failed due to wrong parameters
}

/**
 * @brief Put the NRF module in transmit mode
*/
void NRF24L01_Set_Mode_TX(void) {
  uint8_t Result; // Read out the config register first

	Result = NRF24L01_Read_Reg(CONFIG) & 0x7E; // Read out the config register and clear out the PRIM_RX bit 0
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Put the NRF module in receive mode
*/
void NRF24L01_Set_Mode_RX(void) {
  uint8_t Result; // Read out the config register first

	Result = NRF24L01_Read_Reg(CONFIG) | 0x1; // Read out the config register and set the PRIM_RX bit 0
	NRF24L01_Write_Reg(CONFIG, Result);
}

/**
 * @brief Enables and configures a specified receiving pipe
 *
 * @param PipeNumber  The number of the pipe to be configured
 * @param PipeAddress The address the pipe needs to get
 * @param AddressSize The size of the address
 * @param PayloadSize The size of the payload that will be received in this pipe
 *
*/
void NRF24L01_Set_RX_Pipe(uint8_t PipeNumber, uint8_t *PipeAddress, uint8_t AddressSize, uint8_t PayloadSize) {
	uint8_t Result;
	// Read out the EN_RXADDR register to keep all the active pipes enabled
	Result = NRF24L01_Read_Reg(EN_RXADDR);
	NRF24L01_Write_Reg(EN_RXADDR, Result | (1 << PipeNumber)); // Activate the specified pipe
	// Write the payload size in the appropriate RX_PW register
	NRF24L01_Write_Reg((RX_PW_P0 + PipeNumber), PayloadSize);
	if(PipeNumber <= 1)
	  NRF24L01_Write_Reg_Buf((RX_ADDR_P0 + PipeNumber), PipeAddress, (uint16_t)AddressSize); // Write the PipeAdress
	else
	  NRF24L01_Write_Reg((RX_ADDR_P0 + PipeNumber), PipeAddress[0]);
}

/**
 * @brief Disable all pipes
*/
void NRF24L01_Disable_All_Pipes(void) {
	NRF24L01_Write_Reg(EN_RXADDR, 0);
}

/**
 * @brief Enable all pipes
*/
void NRF24L01_Enable_All_Pipes(void) {
	NRF24L01_Write_Reg(EN_RXADDR, 0x3F);
}

/**
 *
 * @brief Enable the specified interrupt
 *
 * @param Interrupt The interrupt that has to be enabled
 * (RX_DR/TX_DS/MAX_RT)
 *
 */
void NRF24L01_Enable_Interrupt(uint8_t Interrupt){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config |= Interrupt;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Disable the specified interrupt
 *
 * @param Interrupt The interrupt that has to be disabled
 * (RX_DR/TX_DS/MAX_RT)
 *
 */
void NRF24L01_Disable_Interrupt(uint8_t Interrupt){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config &= ~Interrupt;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Enable all the interrupts
 *
 */
void NRF24L01_Enable_All_Interrupts(void){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config |= RX_DR | TX_DS | MAX_RT;
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Disable all interrupts
 *
 */
void NRF24L01_Disable_All_Interrupts(void){
  uint8_t Config; // Variable to read out the config register
  // Read out the current config register value before adjusting it
  Config = NRF24L01_Read_Reg(CONFIG);
  Config &= ~(RX_DR | TX_DS | MAX_RT);
  NRF24L01_Write_Reg(CONFIG, Config);
}

/**
 *
 * @brief Clear all interrupts
 *
 * @return STATUS Register
 *
 */
uint8_t NRF24L01_Clear_All_Interrupts(void) {
	return NRF24L01_Write_Reg(STATUS, RX_DR | TX_DS | MAX_RT);
}

/**
 * @brief Set the transmit address of this module
 *
 * @param TransmitAddress The address
 * @param AddressSize The size of the given address (must be the size set in the SETUP_AW register)
 *
*/
void NRF24L01_Set_TX_Address(uint8_t *TransmitAddress, uint16_t AddressSize) {
	NRF24L01_Write_Reg_Buf(TX_ADDR, TransmitAddress, AddressSize);
}

/**
 * @brief Flush the transmit buffer
*/
void NRF24L01_Flush_TX(void) {
  uint8_t Data = FLUSH_TX;
  CSN_L();
  HAL_SPI_Transmit(&InitSPI, &Data, 1, 1000);
  CSN_H();
}

/**
 * @brief Flush the receive buffer
*/
void NRF24L01_Flush_RX(void) {
  uint8_t Data = FLUSH_RX;
  CSN_L();
  HAL_SPI_Transmit(&InitSPI, &Data, 1, 1000);
  CSN_H();
}

/**
 * @brief Initialize the pins for the NRF module
 */
void Pins_Init(void){
  // Enable the clock for the GPIO peripherals
#ifdef NRF24_GPIOA
  __HAL_RCC_GPIOA_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOB
  __HAL_RCC_GPIOB_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOC
  __HAL_RCC_GPIOC_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOD
  __HAL_RCC_GPIOD_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOE
  __HAL_RCC_GPIOE_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOF
  __HAL_RCC_GPIOF_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOG
  __HAL_RCC_GPIOG_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOH
  __HAL_RCC_GPIOH_CLK_ENABLE();
#endif
#ifdef NRF24_GPIOI
  __HAL_RCC_GPIOI_CLK_ENABLE();
#endif

  // Configure the GPIO pins
  GPIO_InitTypeDef PORT;
  // Configure SPI pins
  PORT.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  PORT.Pin = NRF24_SPI_SCK_PIN | NRF24_SPI_MISO_PIN | NRF24_SPI_MOSI_PIN;
  PORT.Mode = GPIO_MODE_AF_PP;
#ifdef NRF24_SPI5
  PORT.Alternate = GPIO_AF5_SPI5;
#elif defined(NRF24_SPI4)
  PORT.Alternate = GPIO_AF5_SPI4;
#elif defined(NRF24_SPI3)
  PORT.Alternate = GPIO_AF5_SPI3;
#elif defined(NRF24_SPI2)
  PORT.Alternate = GPIO_AF5_SPI2;
#elif defined(NRF24_SPI1)
  PORT.Alternate = GPIO_AF5_SPI1;
#endif
  HAL_GPIO_Init(NRF24_SPI_GPIO_PORT, &PORT);
  // Configure CS pin as output with Push-Pull
  PORT.Pin = NRF24_SPI_CS_PIN;
  PORT.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(NRF24_SPI_GPIO_PORT,&PORT);
  // Configure CE pin as output with Push-Pull
  PORT.Pin = NRF24_CE_PIN;
  PORT.Mode = GPIO_MODE_OUTPUT_PP;
  HAL_GPIO_Init(NRF24_CE_PORT,&PORT);
  // Configure IRQ pin as input with Pull-Up
  PORT.Pin = NRF24_IRQ_PIN;
  PORT.Mode = GPIO_MODE_IT_FALLING;
  PORT.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(NRF24_IRQ_PORT,&PORT);

  CE_L();
}

/**
 * @brief Initialize the SPI peripheral
 *
 * @param Prescaler The prescaler to be used for the spi peripheral (see the hal GPIO header file for prescalers)
 */
void SPI_Init(uint16_t Prescaler){
  // Enable the SPI peripheral clock
#ifdef NRF24_SPI5
  __HAL_RCC_SPI5_CLK_ENABLE();
#elif defined(NRF24_SPI4)
  __HAL_RCC_SPI4_CLK_ENABLE();
#elif defined(NRF24_SPI3)
  __HAL_RCC_SPI3_CLK_ENABLE();
#elif defined(NRF24_SPI2)
  __HAL_RCC_SPI2_CLK_ENABLE();
#elif defined(NRF24_SPI1)
  __HAL_RCC_SPI1_CLK_ENABLE();
#endif

  InitSPI.Instance = NRF24_SPI_PORT; // SPI peripheral to use
  InitSPI.Init.Mode = SPI_MODE_MASTER; // Set in master mode
  InitSPI.Init.BaudRatePrescaler = Prescaler; // Set prescaler
  InitSPI.Init.Direction = SPI_DIRECTION_2LINES; // Bidirectional normal SPI bus
  InitSPI.Init.CLKPolarity = SPI_POLARITY_LOW; // Low when idle
  InitSPI.Init.CLKPhase = SPI_PHASE_1EDGE;
  InitSPI.Init.CRCPolynomial = 7;
  InitSPI.Init.DataSize = SPI_DATASIZE_8BIT; // 8-bit per transfer
  InitSPI.Init.FirstBit = SPI_FIRSTBIT_MSB; // Send MSB first
  InitSPI.Init.NSS = SPI_NSS_HARD_OUTPUT; // Let the peripheral handle the chip-select
  //InitSPI.Init.NSSPMode = SPI_NSS_PULSE_ENABLE;
  // Init the SPI unit
  HAL_SPI_MspInit(&InitSPI);
  HAL_SPI_Init(&InitSPI);
}

/**
 * @brief Init routine to initialize the Pins and SPI peripheal and do a basic setup of the module
 * 
 * With this function all the pins are configured and the SPI is also configured.
 * The NRF module is loaded with basic settings that can be configured using the input paramters
 * of this function. Further configuration of the device can be done with all the other functions
 * available.
 * What is standard set with this function:
 * Shockburst is enabled for all channels
 * The transmit address is set togheter with a receive pipe for auto acknowledgement
 * The device is put in transmit mode
 * The device is powered up
 * Other things are configured with parameters, see below
 *
 * @param Channel         The channel to be set for receive and transmit (0..125)
 * @param DataRate        The data rate to be used (1Mbps, 2Mbps, 250Kbps)
 * @param TransmitAddress The address to be used for transmitting 
 * @param AddressWidth    The width of the address (3..5)
 * @param PayloadSize     The size of the payloads to be transmitted
 *
*/
void NRF24L01_Init(uint8_t Channel, uint8_t DataRate,
		uint8_t *TransmitAddress, uint8_t AddressWidth, uint8_t PayloadSize) {
  // Initalize the pins and the SPI peripheral
  Pins_Init();
	SPI_Init(SPI_BAUDRATEPRESCALER_64);

	// Enable Enhanced ShockBurst for all channels
	NRF24L01_Enable_AutoACK(0x3F);

	// Set the maximum output power (0dBm) and set the datarate
	NRF24L01_Write_Reg(RF_SETUP, (0x6 | DataRate));
	// Set the used overal address width
	NRF24L01_Set_Address_Width(AddressWidth);
	// To use Auto acknowledgement, pipe 0 must have the transmit address
	NRF24L01_Set_RX_Pipe(0, TransmitAddress, AddressWidth, PayloadSize);
	// The channel to be used for tranmitting and receiving
	NRF24L01_Set_Channel(Channel);
	NRF24L01_Set_TX_Address(TransmitAddress, AddressWidth); // Set Transmit address

	// Enable CRC, set CRC to 2 bytes, put into transmit mode and power up the module
	NRF24L01_Write_Reg(CONFIG, 0xE);

	HAL_Delay(2); // Delay for 2ms (1,5ms specified) after module power-up
}

/**
 Turn on transmitter, and transmits the data loaded into the buffer
*/
void NRF24L01_Send_Payload(uint8_t *Buf, uint16_t BufSize) {
  uint8_t Result;
  
  if(NRF24L01_Read_Status_Reg() & MAX_RT) // Clear all interrupts if MAX_RT is set
    NRF24L01_Clear_All_Interrupts();
  // Make sure the device is in TX_MODE and power it up
  Result = NRF24L01_Read_Reg(CONFIG) & 0x7E;
  NRF24L01_Write_Reg(CONFIG, (Result | 0x2));
  
  NRF24L01_Write_TX_Buf(Buf, BufSize);
  
  // Let the CE-pin go high for at least 10us
	CE_L();
	CE_H();
	HAL_Delay(1); // Delay 1ms (10us is specified but the HAL minimum delay is in ms)
	              // Also I am too lazy to implement my own delay based on timers
	CE_L();
}

/**
 * @brief Transmit a payload (this function automaticly puts the module in TX mode)
 *
 * @param Buf       The payload that has to be transmitted
 * @param BufSize   The size of the payload to be transmitted
 *
 * @return Returns 1 on success and 0 when transmit fails
 *
*/
uint8_t NRF24L01_Send(uint8_t *Buf, uint16_t BufSize) {
  NRF24L01_Set_Mode_TX(); // Go into transmit mode
  // Send the payload
  NRF24L01_Send_Payload(Buf, BufSize);
  //Wait for the payload to be send
  while ((NRF24L01_Read_Status_Reg() & TX_DS) != TX_DS){
      if((NRF24L01_Read_Status_Reg() & MAX_RT) == MAX_RT){
          NRF24L01_Clear_All_Interrupts();
          NRF24L01_Flush_TX();
          return 0; //Failed
      }
  }
  return 1; // Success
  NRF24L01_Clear_All_Interrupts();

}

/**
 * @brief Write the transmit payload buffer
 *
 * @param PayloadBuffer The data to be written into the payload buffer
 * @param PayloadSize   The size of the data to be written into the buffer
 *
*/
void NRF24L01_Write_TX_Buf(uint8_t *PayloadBuffer, uint16_t PayloadSize) {
	NRF24L01_Write_Reg_Buf(W_TX_PAYLOAD, PayloadBuffer, PayloadSize);
}

/**
 * @brief Read the data from the receive payload buffer
 *
 * @param ReceiveBuf  Data received
 * @param PayloadSoze Size of the received data
 *
*/
void NRF24L01_Read_RX_Buf(uint8_t *ReceiveBuf, uint16_t PayloadSize) {
	NRF24L01_Read_Reg_Buf(R_RX_PAYLOAD, ReceiveBuf, PayloadSize);
}
