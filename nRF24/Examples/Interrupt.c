/*  Example code to receive data with the nRF24 module interrupt method */

// Include MCU specific libs
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
// Include the nRF24 lib (duh)
#include "nrf24l01.h"

// Global variables
uint8_t NrfDataReady = 0;

void main(void)
{

  // Receive buffer
  uint8_t RxBuf[_Buffer_Size] = { 0x1, 0x2, 0x3, 0x4 };
  // The transmit address
  uint8_t TX_Address[_Address_Width] = { 0xD2, 0xF0, 0xF0, 0xF0, 0xF0 };
  // The nRF24 module can receive from 4 different modules with different addresses (pipes)
  uint8_t Module1_Address[_Address_Width] = { 0xE1, 0xF0, 0xF0, 0xF0, 0xF0 };
  uint8_t Module2_Address = 0xE2;
  uint8_t Module3_Address = 0xE3;
  uint8_t Module4_Address = 0xE4;

  // Init the NRF24 unit with basic parameters
  NRF24L01_Init(_Channel, _1Mbps, TX_Address, _Address_Width, _Buffer_Size);
  // Setup all the receiving pipes
  NRF24L01_Set_RX_Pipe(1, Module1_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(2, &Module2_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(3, &Module3_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(4, &Module4_Address, _Address_Width, _Buffer_Size);
  // Flush the buffers at setup
  NRF24L01_Flush_TX();
  NRF24L01_Flush_RX();
  // Clear all interrupts
  NRF24L01_Clear_All_Interrupts();

  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 9, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn); // Enable interrupts lines 10-15

  NRF24L01_Set_Mode_RX(); // Go into receive mode
  CE_H(); // Place the CE pin high to start receiving

  while(1){
    // Read data from the NRF module if it's available
    if (NrfDataReady){
      CE_L(); // Stop receiving
      NRF24L01_Read_RX_Buf(RxBuf, _Buffer_Size);
      NRF24L01_Clear_All_Interrupts();
      // Reset variable
      NrfDataReady = 0;
      CE_H(); // Start receiving again
    }
  }

}

// I/O-lines 10-15 IRQ handler
void EXTI15_10_IRQHandler(void) {
  uint8_t Status; // Store the status register contents

  // If the IRQ pin is high
  if (!HAL_GPIO_ReadPin(NRF24_IRQ_PORT, NRF24_IRQ_PIN)) {
    // Read out the NRF24 status register
    Status = NRF24L01_Read_Status_Reg();
    // Else if structure to handle all events
    if(Status & RX_DR) // When there is data ready in the receive FIFO
      NrfDataReady = Status & RX_P_NO; // Load the variable with the receive pipe
    else if(Status & TX_DS) // When the data was sent successfully after a transmit operation
      __NOP();
    else if(Status & MAX_RT) // The data was not sent after the maximum configured amount of attempts
      __NOP();
  }

  // Interrupt handler
  HAL_GPIO_EXTI_IRQHandler(NRF24_IRQ_PIN);
}
