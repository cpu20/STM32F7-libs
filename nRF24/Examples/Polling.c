/*  Example code to receive data with the nRF24 module polling method */

// Include MCU specific libs
#include "stm32f7xx.h"
#include "stm32f7xx_hal.h"
// Include the nRF24 lib (duh)
#include "nrf24l01.h"


/**
 * @brief Function constantly monitors for new data should only be used
 *        for testing as it locks up the whole program (unless threaded)
 * @param Buf buffer to store the received data
 */
void NRF24L01_Receive(uint8_t Buf[_Buffer_Size]) {
  uint8_t Result;
  uint16_t Delay;

  NRF24L01_Set_Mode_RX(); // Go into receive mode

  CE_H(); // Place the CE pin high to start receiving

  // Check the status register to see if anything was received
  Result = (NRF24L01_Read_Status_Reg() & RX_DR);

  while (Result != RX_DR) {
    Delay = 0xFF;
    Result = (NRF24L01_Read_Status_Reg() & RX_DR);
    while (Delay--);
  }
  // End of receive
  CE_L();
  // Read the received data
  NRF24L01_Read_RX_Buf(Buf, _Buffer_Size);
  NRF24L01_Clear_All_Interrupts();
}

void main(void)
{

  // Receive buffer
  uint8_t RxBuf[_Buffer_Size] = { 0x1, 0x2, 0x3, 0x4 };
  // The transmit address
  uint8_t TX_Address[_Address_Width] = { 0xD2, 0xF0, 0xF0, 0xF0, 0xF0 };
  // The nRF24 module can receive from 4 different modules with diffrent addresses (pipes)
  uint8_t Module1_Address[_Address_Width] = { 0xE1, 0xF0, 0xF0, 0xF0, 0xF0 };
  uint8_t Module2_Address = 0xE2;
  uint8_t Module3_Address = 0xE3;
  uint8_t Module4_Address = 0xE4;

  // Init the NRF24 unit with basic parameters
  NRF24L01_Init(_Channel, _1Mbps, TX_Address, _Address_Width, _Buffer_Size);
  // Setup all the receiving pipes
  NRF24L01_Set_RX_Pipe(1, Module1_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(2, &Module2_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(3, &Module3_Address, _Address_Width, _Buffer_Size);
  NRF24L01_Set_RX_Pipe(4, &Module4_Address, _Address_Width, _Buffer_Size);
  // Flush the buffers at setup
  NRF24L01_Flush_TX();
  NRF24L01_Flush_RX();
  // Clear all interrupts
  NRF24L01_Clear_All_Interrupts();
  // Start receiving (polling method)
  while(1){
    NRF24L01_Receive(RxBuf);
  }

}
