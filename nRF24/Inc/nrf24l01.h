/*  This code is used to drive NRF24 wireless modules on a STM32F7 mcu */

#ifndef _NRF24L01_H_
#define _NRF24L01_H_

#include "stm32f7xx_hal.h" // MCU Specific: stm32f7xx, stm32f4xx, stm32f1xx etc.
#include "stm32f7xx_hal_gpio.h"
#include "stm32f7xx_hal_spi.h"
#include <stdint.h>

// Used SPI and GPIO port defines
#define NRF24_SPI5
#define NRF24_GPIOF
#define NRF24_GPIOB

#define NRF24_SPI_PORT      SPI5          //Used SPI_PORT
#define NRF24_SPI_SCK_PIN   GPIO_PIN_7    // PF7
#define NRF24_SPI_MISO_PIN  GPIO_PIN_8    // PF8
#define NRF24_SPI_MOSI_PIN  GPIO_PIN_9    // PF9
#define NRF24_SPI_CS_PIN    GPIO_PIN_6    // PF6
#define NRF24_SPI_GPIO_PORT GPIOF

// nRF24L01 CE (Chip Enable) pin
#define NRF24_CE_PORT     GPIOB
#define NRF24_CE_PIN      GPIO_PIN_14    // PB14

// nRF24L01 IRQ pin
#define NRF24_IRQ_PORT    GPIOB
#define NRF24_IRQ_PIN     GPIO_PIN_15    // PB15

// Chip Enable Activates RX or TX mode
#define CE_L() HAL_GPIO_WritePin(NRF24_CE_PORT, NRF24_CE_PIN, GPIO_PIN_RESET)
#define CE_H() HAL_GPIO_WritePin(NRF24_CE_PORT, NRF24_CE_PIN, GPIO_PIN_SET)

// SPI Chip Select
#define CSN_L() HAL_GPIO_WritePin(NRF24_SPI_GPIO_PORT, NRF24_SPI_CS_PIN, GPIO_PIN_RESET)
#define CSN_H() HAL_GPIO_WritePin(NRF24_SPI_GPIO_PORT, NRF24_SPI_CS_PIN, GPIO_PIN_SET)

// Fixed configurations
#define _Channel 0x4C     /* Channel 0..125 */
#define _Address_Width 5 /* 3..5 */
#define _Buffer_Size 4 /* 1..32 */

// Bits

/**
 * Data Ready RX FIFO interrupt
 */
#define RX_DR (1<<6)

/**
 * Data Sent TX FIFO interrupt
 */
#define TX_DS (1<<5)

/**
 * Maximum number of TX retransmits interrupt
 */
#define MAX_RT (1<<4)

/** Power Down mode
 *
 * Minimal current consumption, SPI can be activated
 *
 * @see NRF24L01_Set_Power(uint8_t Mode)
 */
#define POWER_DOWN	0

/** Power Up mode
 *
 * Standby-I mode
 *
 * @see NRF24L01_Set_Power(uint8_t Mode)
 */
#define POWER_UP	(1<<1)

/** Mode radio transmitter
 *
 * @see NRF24L01_Set_Device_Mode(uint8_t Device_Mode)
 * @see NRF24L01_Init(uint8_t Device_Mode, uint8_t CH, uint8_t DataRate,
		uint8_t *Address, uint8_t Address_Width, uint8_t Size_Payload)
 */
#define TX_MODE	0

/** Mode radio receiver
 *
 * @see NRF24L01_Set_Device_Mode(uint8_t Device_Mode)
 * @see NRF24L01_Init(uint8_t Device_Mode, uint8_t CH, uint8_t DataRate,
		uint8_t *Address, uint8_t Address_Width, uint8_t Size_Payload)
 */
#define RX_MODE	1

/**  Air data rate = 250 Kbps
 *
 *
 * @see NRF24L01_Init(uint8_t Device_Mode, uint8_t CH, uint8_t DataRate,
    uint8_t *Address, uint8_t Address_Width, uint8_t Size_Payload)
 */
#define _250Kbps  (1<<5)

/**  Air data rate = 1 Mbps
 *
 *
 * @see NRF24L01_Init(uint8_t Device_Mode, uint8_t CH, uint8_t DataRate,
		uint8_t *Address, uint8_t Address_Width, uint8_t Size_Payload)
 */
#define	_1Mbps	0

/** Air data rate = 2 Mbps
 *
 * @see NRF24L01_Init(uint8_t Device_Mode, uint8_t CH, uint8_t DataRate,
		uint8_t *Address, uint8_t Address_Width, uint8_t Size_Payload)
 */
#define	_2Mbps	(1<<3)

/** Enable ShockBurst

 Automatic Retransmission (Up to 1 Re-Transmit on fail of AA)

 Auto Acknowledgment (data pipe 0)

 @see NRF24L01_Set_ShockBurst(uint8_t Mode)
 */
#define ShockBurst_ON 1

/** Disable ShockBurst
 *
 @see NRF24L01_Set_ShockBurst(uint8_t Mode)
 */
#define ShockBurst_OFF 0

/**
 *
 * Data pipe number for the payload available for
 * reading from
 * RX_FIFO
 * 000-101: Data Pipe Number
 * 110: Not Used
 * 111: RX FIFO Empty
 *
 */
#define RX_P_NO 0xE

#define _0dBm 0x3

#define _6dBm 0x2

#define _12dBm 0x1

#define _18dBm 0


// REGISTERS
#define CONFIG		0x00
#define EN_AA		0x01
#define EN_RXADDR	0x02
#define SETUP_AW	0x03
#define SETUP_RETR	0x04
#define RF_CH		0x05
#define RF_SETUP	0x06
#define STATUS		0x07
#define OBSERVE_TX	0x08
#define RPD		0x09
#define RX_ADDR_P0	0x0A
#define RX_ADDR_P1	0x0B
#define RX_ADDR_P2	0x0C
#define RX_ADDR_P3	0x0D
#define RX_ADDR_P4	0x0E
#define RX_ADDR_P5	0x0F
#define TX_ADDR		0x10
#define RX_PW_P0	0x11
#define RX_PW_P1	0x12
#define RX_PW_P2	0x13
#define RX_PW_P3	0x14
#define RX_PW_P4	0x15
#define RX_PW_P5	0x16
#define FIFO_STATUS	0x17
#define DYNPD		0x1C
#define FEATURE		0x1D

// COMMANDS
#define R_REGISTER	    0x00
#define W_REGISTER	    0x20
#define R_RX_PAYLOAD        0x61
#define W_TX_PAYLOAD        0xA0
#define FLUSH_TX      	    0xE1
#define FLUSH_RX	    0xE2
#define REUSE_TX_PL  	    0xE3
#define ACTIVATE            0x50
#define R_RX_PL_WID         0x60
#define W_ACK_PAYLOAD	    0xA8
#define W_TX_PAYLOAD_NOACK  0x58
#define NOP                 0xFF

/*
 * SPI functions for NRF24L01
 */
uint8_t NRF24L01_Read_Reg(uint8_t Reg);
uint8_t NRF24L01_Write_Reg(uint8_t Reg, uint8_t Value);
uint8_t NRF24L01_Read_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize);
uint8_t NRF24L01_Write_Reg_Buf(uint8_t Reg, uint8_t *Buf, uint16_t RegSize);

/*
 * NRF24L01 functions
 */
uint8_t NRF24L01_Read_Status_Reg(void);
uint8_t NRF24L01_Read_RPD(void);
void NRF24L01_Power_Down(void);
void NRF24L01_Power_Up(void);
void NRF24L01_Power_Down(void);
void NRF24L01_Power_Up(void);
void NRF24L01_Set_Channel(uint8_t Channel);
void NRF24L01_Set_Power(uint8_t Power);
void NRF24L01_Set_Data_Rate(uint8_t DataRate);
uint8_t NRF24L01_Set_Address_Width(uint8_t AddressWidth);
void NRF24L01_Set_Mode_TX(void);
void NRF24L01_Set_Mode_RX(void);
void NRF24L01_Set_RX_Pipe(uint8_t PipeNumber, uint8_t *PipeAddress, uint8_t AddressSize, uint8_t PayloadSize);
void NRF24L01_Disable_All_Pipes(void);
void NRF24L01_Enable_All_Pipes(void);
void NRF24L01_Enable_Interrupt(uint8_t Interrupt);
void NRF24L01_Disable_Interrupt(uint8_t Interrupt);
void NRF24L01_Enable_All_Interrupts(void);
void NRF24L01_Disable_All_Interrupts(void);
uint8_t NRF24L01_Clear_All_Interrupts(void);
void NRF24L01_Set_TX_Address(uint8_t *TransmitAddress, uint16_t AddressSize);
void NRF24L01_Flush_TX(void);
void NRF24L01_Flush_RX(void);
void NRF24L01_Init(uint8_t Channel, uint8_t DataRate,
                   uint8_t *TransmitAddress, uint8_t AddressWidth, uint8_t PayloadSize);
void NRF24L01_Send_Payload(uint8_t *Buf, uint16_t BufSize);
uint8_t NRF24L01_Send(uint8_t *Buf, uint16_t BufSize);
void NRF24L01_Write_TX_Buf(uint8_t *PayloadBuffer, uint16_t PayloadSize);
void NRF24L01_Read_RX_Buf(uint8_t *ReceiveBuf, uint16_t PayloadSize);

#endif
